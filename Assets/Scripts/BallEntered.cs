﻿using UnityEngine;

public class BallEntered : MonoBehaviour {
    public GameObject ball;
    public Material solvedMat;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == ball)
        {
            SendMessageUpwards("BallEntered");
            GetComponent<Renderer>().material = solvedMat;
        }
    }
}
