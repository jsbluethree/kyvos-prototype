﻿using UnityEngine;

public class ClickButton : MonoBehaviour {
    public Vector3 displacement;
    public Vector3 startPos;
    public float totalTime;
    private float speed;
    public float currentTime;
    public bool useDefaultAnim;

    void Start()
    {
        if (useDefaultAnim)
        {
            startPos = gameObject.transform.localPosition;
            speed = 2 * displacement.magnitude / totalTime;
            currentTime = totalTime + 1;
        }
    }

    void Update()
    {
        if (useDefaultAnim)
        {

            if (currentTime <= totalTime / 2)
            {
                gameObject.transform.localPosition = Vector3.Lerp(startPos, startPos + displacement, currentTime * speed);
            }
            else if (currentTime <= totalTime)
            {
                gameObject.transform.localPosition = Vector3.Lerp(startPos + displacement, startPos, (currentTime - (totalTime / 2)) * speed);
            }
            else
            {
                gameObject.transform.localPosition = startPos;
            }
            currentTime += Time.deltaTime;
        }
    }

    void OnMouseDown()
    {
        if (useDefaultAnim)
        {
            if (currentTime > totalTime / 2)
            {
                SendMessageUpwards("Clicked", gameObject);
                if (currentTime <= totalTime)
                {
                    currentTime = totalTime - currentTime;
                }
                else
                {
                    currentTime = 0;
                }
            }
        }
        else
        {
            SendMessageUpwards("Clicked", gameObject);
        }
    }
}
