﻿using UnityEngine;

public class ModuleSelect : MonoBehaviour {
    public GameObject parent;
    private Quaternion rotation;
    public float speed = 300.0f;
    public bool rotating;

	void Start()
    {
        rotation = Quaternion.FromToRotation(GetComponent<BoxCollider>().center.normalized, Vector3.back);
	}

    void Update()
    {
        if (rotating)
        {
            if (parent.transform.rotation == rotation)
            {
                rotating = false;
            }
            else
            {
                float step = speed * Time.deltaTime;
                parent.transform.rotation = Quaternion.RotateTowards(parent.transform.rotation, rotation, step);
            }
        }
    }

	void OnMouseDown()
    {
        rotating = true;
    }
}