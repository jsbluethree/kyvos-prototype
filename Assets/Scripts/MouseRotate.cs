﻿using UnityEngine;

public class MouseRotate : MonoBehaviour {
    public float rotationSpeed = 260000f;

	void Update()
    {
        if (Input.GetMouseButton(1))
        {
            float speedMod = Time.deltaTime * rotationSpeed / Screen.width;
            float rotX = Input.GetAxis("Mouse X") * speedMod;
            float rotY = Input.GetAxis("Mouse Y") * speedMod;
            transform.RotateAround(Vector3.zero, Vector3.down, rotX);
            transform.RotateAround(Vector3.zero, Vector3.right, rotY);
        }
	}
}
