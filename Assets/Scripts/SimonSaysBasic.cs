﻿using UnityEngine;

public class SimonSaysBasic : MonoBehaviour
{
    public Material solvedMat;
    public int maxLength = 8;
    public int length = 0;
    public GameObject[] sequence;
    private GameObject[] buttons;
    public int current = 0;
    public float buttonCooldown = 0;
    public float hintTime = 0;
    public float idleTime = 0;
    public int hintShowed = 0;
    public bool solved = false;
    
	void Start()
    {
        sequence = new GameObject[maxLength];
        buttons = GameObject.FindGameObjectsWithTag("SimonButton");
        Random.seed = System.DateTime.Now.GetHashCode();
        sequence[length++] = buttons[Random.Range(0, 4)];
	}
	
	void Update()
    {
        if (buttonCooldown > 0)
        {
            buttonCooldown -= Time.deltaTime;
        }

        // show hint sequence
        if (idleTime >= 5 && !solved)
        {
            if (hintShowed == length)
            {
                idleTime = 0;
                hintTime = 0;
                hintShowed = 0;
            }
            else if (hintTime >= 0.5)
            {
                sequence[hintShowed++].AddComponent<SimonBlinkAnim>();
                hintTime -= 0.5f;
            }
            hintTime += Time.deltaTime;
        }
        else
        {
            idleTime += Time.deltaTime;
        }
    }

    void Clicked(GameObject obj)
    {
        if (buttonCooldown <= 0)
        {
            buttonCooldown = 0.4f;
            if (solved)
            {
                obj.AddComponent<SimonDonePressAnim>();
            }
            else
            {
                obj.AddComponent<SimonPressAnim>();
                // play sound
                if (obj == sequence[current])
                {
                    ++current;
                }
                else
                {
                    current = 0;
                }
                if (current == length)
                {
                    if (length == maxLength)
                    {
                        solved = true;
                        SendMessageUpwards("OnSolve");
                        foreach (GameObject button in buttons)
                        {
                            Renderer rend = button.GetComponent<Renderer>();
                            rend.material = solvedMat;
                        }
                    }
                    else
                    {
                        current = 0;
                        sequence[length++] = buttons[Random.Range(0, 4)];
                        idleTime = 4;
                    }
                }
            }
        }
    }
}

public class SimonPressAnim : MonoBehaviour
{
    public float time = 0;
    public float speed = 20;
    public Vector3 displacement = new Vector3(0.05f, 0, 0);
    public Vector3 startPos;
    private Renderer rend;
    private Color emColor;

    void Start()
    {
        rend = GetComponent<Renderer>();
        startPos = transform.localPosition;
    }

    void Update()
    {
        if (time == 0)
        {
            emColor = rend.material.GetColor("_EmissionColor");
            rend.material.SetColor("_EmissionColor", emColor * 2);
        }
        if (time < 0.25)
        {
            transform.localPosition = Vector3.Lerp(startPos, startPos + displacement, speed * time);
        }
        else if (time < 0.5)
        {
            transform.localPosition = Vector3.Lerp(startPos + displacement, startPos, speed * (time - 0.25f));
        }
        else
        {
            transform.localPosition = startPos;
            emColor = rend.material.GetColor("_EmissionColor");
            rend.material.SetColor("_EmissionColor", emColor * 0.5f);
            Destroy(this);
        }
        time += Time.deltaTime;
    }
}

public class SimonBlinkAnim : MonoBehaviour
{
    public float time = 0;
    private Renderer rend;
    private Color emColor;

    void Start()
    {
        rend = GetComponent<Renderer>();
        emColor = rend.material.GetColor("_EmissionColor");
        rend.material.SetColor("_EmissionColor", emColor * 2);
    }

    void Update()
    {
        if (time > 0.25)
        {
            emColor = rend.material.GetColor("_EmissionColor");
            rend.material.SetColor("_EmissionColor", emColor * 0.5f);
            Destroy(this);
        }
        time += Time.deltaTime;
    }
}

public class SimonDonePressAnim : MonoBehaviour
{
    public float time = 0;
    public float speed = 20;
    public Vector3 displacement = new Vector3(0.05f, 0, 0);
    public Vector3 startPos;

    void Start()
    {
        startPos = transform.localPosition;
    }

    void Update()
    {
        if (time < 0.25)
        {
            transform.localPosition = Vector3.Lerp(startPos, startPos + displacement, speed * time);
        }
        else if (time < 0.5)
        {
            transform.localPosition = Vector3.Lerp(startPos + displacement, startPos, speed * (time - 0.25f));
        }
        else
        {
            transform.localPosition = startPos;
            Destroy(this);
        }
        time += Time.deltaTime;
    }
}