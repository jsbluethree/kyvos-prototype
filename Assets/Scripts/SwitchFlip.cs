﻿using UnityEngine;

public class SwitchFlip : MonoBehaviour {
    public int index;
    void OnMouseDown()
    {
        SendMessageUpwards("Flicked", index);
    }
}
