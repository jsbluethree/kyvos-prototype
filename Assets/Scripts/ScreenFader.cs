﻿using UnityEngine;
using System.Collections;

public class ScreenFader : MonoBehaviour {
    public float fadeSpeed = 1.5f;
    public bool fadingToClear = true;
    public bool fadingToWhite = false;
    private Renderer rend;

    void Start()
    {
        rend = gameObject.GetComponent<Renderer>();
    }

    void Update()
    {
        if (fadingToClear) { FadeIn(); }
        if (fadingToWhite) { FadeOut(); }
    }

    void FadeToClear()
    {
        rend.material.color = Color.Lerp(rend.material.color, Color.clear, fadeSpeed * Time.deltaTime);
    }

    void FadeToBlack()
    {
        rend.material.color = Color.Lerp(rend.material.color, Color.black, fadeSpeed * Time.deltaTime);
    }

    void FadeToWhite()
    {
        rend.material.color = Color.Lerp(rend.material.color, Color.white, fadeSpeed * Time.deltaTime);
    }

    void FadeIn()
    {
        FadeToClear();
        if (rend.material.color.a <= 0.05f)
        {
            rend.material.color = Color.clear;
            rend.enabled = false;
            fadingToClear = false;
        }
    }

    void FadeOut()
    {
        rend.enabled = true;
        FadeToWhite();
        if (rend.material.color.a >= 0.95f)
        {
            rend.material.color = Color.white;
            fadingToClear = true;
            fadingToWhite = false;
        }
    }
}
