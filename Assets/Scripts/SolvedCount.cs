﻿using UnityEngine;

public class SolvedCount : MonoBehaviour {
    public int count = 0;
    public GameObject endCube;
    public bool autosolve;
    private bool done = false;
    private ScreenFader fader;

    void Start()
    {
        fader = GameObject.FindGameObjectWithTag("Fader").GetComponent<ScreenFader>();
    }

    void Update()
    {
        if (!done)
        {
            if (autosolve) { OnSolve(); autosolve = false; }
            if (count == 6) { fader.fadingToWhite = true; count = 7; }
            if (count > 6 && fader.fadingToClear)
            {
                Instantiate(endCube, transform.position, transform.rotation);
                done = true;
                Destroy(gameObject);
            }
        }
        
    }
	
    void OnSolve()
    {
        ++count;        
    }
}
