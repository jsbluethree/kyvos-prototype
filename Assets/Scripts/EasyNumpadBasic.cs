﻿using UnityEngine;

public class EasyNumpadBasic : MonoBehaviour
{
    public Material blank;
    public Material star;
    public Material litStar;
    public Material litRedStar;
    public GameObject submit;
    public GameObject clear;
    public int numOn = 0;
    public int attempts = 0;
    public int maxAttempts = 10;
    public float submitCooldown = 0;
    public float buttonCooldown = 0;
    public bool solved;
    public GameObject[] screens;
    private Renderer[] screenRends = new Renderer[4];

    public void Start()
    {
        for (int i = 0; i < 4; ++i)
        {
            screenRends[i] = screens[i].GetComponent<Renderer>();
        }
    }

    public void Update()
    {
        if (submitCooldown > 0)
        {
            submitCooldown -= Time.deltaTime;
        }
        if (buttonCooldown > 0)
        {
            buttonCooldown -= Time.deltaTime;
        }
    }

    public void Clicked(GameObject obj)
    {
        if (buttonCooldown <= 0)
        {
            obj.AddComponent<NumPadPressAnim>();
            buttonCooldown = 0.2f;
            if (submitCooldown <= 0 && !solved)
            {
                if (obj == submit)
                {
                    if (numOn == 4)
                    {
                        if (++attempts == maxAttempts)
                        {
                            solved = true;
                            Debug.Log("numpad solved");
                            SendMessageUpwards("OnSolve");
                        }
                        for (int i = 0; i < 4; ++i)
                        {
                            NumpadLightAnim anim = screens[i].AddComponent<NumpadLightAnim>();
                            anim.rend = screenRends[i];
                            if (!solved)
                            {
                                anim.lit = litRedStar;
                                anim.unlit = blank;
                            }
                            else
                            {
                                anim.lit = litStar;
                                anim.unlit = litStar;
                            }
                        }
                        numOn = 0;
                        submitCooldown = 1;
                    }
                }
                else if (obj == clear)
                {
                    foreach (Renderer rend in screenRends)
                    {
                        rend.material = blank;
                        numOn = 0;
                    }
                }
                else if (numOn < 4)
                {
                    screenRends[numOn++].material = star;
                }
            }
        }
    }
}

public class NumpadLightAnim : MonoBehaviour
{
    public float time = 0;
    public Renderer rend;
    public Material unlit;
    public Material lit;

    void Start()
    {
        rend.material = lit;
    }

    void Update()
    {
        if (time > 1)
        {
            rend.material = unlit;
            Destroy(this);
        }
        time += Time.deltaTime;
    }
}

public class NumPadPressAnim : MonoBehaviour
{
    public float time = 0;
    public float speed = 40;
    public Vector3 displacement = new Vector3(0, 0, 0.025f);
    public Vector3 startPos;

    void Start()
    {
        startPos = transform.localPosition;
    }

    void Update()
    {
        if (time < 0.125)
        {
            transform.localPosition = Vector3.Lerp(startPos, startPos + displacement, speed * time);
        }
        else if (time < 0.2)
        {
            transform.localPosition = Vector3.Lerp(startPos + displacement, startPos, speed * (time - 0.125f));
        }
        else
        {
            transform.localPosition = startPos;
            Destroy(this);
        }
        time += Time.deltaTime;
    }
}