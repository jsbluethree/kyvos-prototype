﻿using UnityEngine;

public class TimingBasic : MonoBehaviour {
    public Material lightOn;
    public Material lightOff;
    public Material solvedMat;
    public GameObject lightObj;
    private Renderer lightRend;
    public bool lightIsOn = false;
    public bool pressed = false;
    public float timer;
    public int count = 0;
    public int maxCount = 20;
    public bool solved = false;

    void Start()
    {
        lightRend = lightObj.GetComponent<Renderer>();
    }
    	
	void Update()
    {
        if (!solved)
        {
            if (timer >= .5)
            {
                lightRend.material = lightOn;
                lightIsOn = true;
            }
            if (timer >= .75)
            {
                lightRend.material = lightOff;
                lightIsOn = false;
                timer -= 0.75f;
                if (!pressed) count = 0;
                pressed = false;
            }
            timer += Time.deltaTime;
        }
        else
        {
            lightRend.material = solvedMat;
        }
	}

    void Clicked(GameObject obj)
    {
        if (!solved)
        {
            if (lightIsOn)
            {
                ++count;
                pressed = true;
            }
            if (count == maxCount)
            {
                solved = true;
                SendMessageUpwards("OnSolve");
            }
        }
    }
}