﻿using UnityEngine;

public class LetterButton : MonoBehaviour {
    public int index;

    void OnMouseDown()
    {
        SendMessageUpwards("Clicked", index);
    }
}
