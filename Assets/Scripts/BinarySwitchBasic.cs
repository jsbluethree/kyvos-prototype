﻿using UnityEngine;

public class BinarySwitchBasic : MonoBehaviour {
    public GameObject[] switches;
    public GameObject[] lights;
    private Renderer[] lightRends = new Renderer[3];
    public bool[] lightsOn = new bool[3];
    public Material onMat;
    public Material offMat;
    public Material solvedMat;
    public GameObject top;
    public int value = 0;
    public int[] values;
    public bool solved = false;
    private SolvedCount counter;

    void Start()
    {
        for (int i = 0; i < 3; ++i)
        {
            lightRends[i] = lights[i].GetComponent<Renderer>();
        }
        counter = top.GetComponent<SolvedCount>();
        values = new int[]{ 1, 2, 4 };
    }

    void Flicked(int index)
    {
        if (!solved)
        {
            if (lightsOn[index])
            {
                lightRends[index].material = offMat;
                lightsOn[index] = false;
                value -= values[index];
            }
            else
            {
                lightRends[index].material = onMat;
                lightsOn[index] = true;
                value += values[index];
            }
        }
        switches[index].transform.Rotate(0, 0, 180);
    }

    void Clicked(GameObject obj)
    {
        obj.AddComponent<BinPressAnim>();
        if (!solved)
        {
            if (counter.count == value)
            {
                solved = true;
                SendMessageUpwards("OnSolve");
                for (int i = 0; i < 3; ++i)
                {
                    lightRends[i].material = solvedMat;
                }
            }
        }
    }
}

public class BinPressAnim : MonoBehaviour
{
    public float time;

    void Update()
    {
        time += Time.deltaTime;
        if (time >= 0.5)
            Destroy(this);
    }
}