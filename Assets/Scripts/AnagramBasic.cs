﻿using UnityEngine;

public class AnagramBasic : MonoBehaviour {
    public GameObject[] buttons;
    public GameObject[] screens;
    private Renderer[] screenRends = new Renderer[8];
    public Material[] letterMats;
    public Material[] solvedMats;
    public Material blankMat;
    public Material wrongMat;
    public int pressed = 0;
    private string chars ="aggnuwzz";
    private string solution = "zugzwang";
    public string input = "";
    public bool solved = false;
    public float cooldown = 0;

    void Start()
    {
        for (int i = 0; i < 8; ++i)
        {
            screenRends[i] = screens[i].GetComponent<Renderer>();
        }
    }

    void Update()
    {
        if (cooldown > 0)
        {
            if (cooldown <= Time.deltaTime)
            {
                for (int i = 0; i < 8; ++i)
                {
                    screenRends[i].material = blankMat;
                }
            }
            cooldown -= Time.deltaTime;
        }
    }
    
	void Clicked(int index)
    {
        if (!solved && cooldown <= 0)
        {
            screenRends[pressed++].material = letterMats[index];
            input += chars[index];
            buttons[index].transform.Translate(0.02f, 0, 0);
            if (pressed == 8)
            {
                if (solution == input)
                {
                    solved = true;
                    SendMessageUpwards("OnSolve");
                    for (int i = 0; i < 8; ++i)
                    {
                        screenRends[i].material = solvedMats[i];
                    }
                }
                else
                {
                    for (int i = 0; i < 8; ++i)
                    {
                        screenRends[i].material = wrongMat;
                        cooldown = 1.5f;
                        pressed = 0;
                        input = "";
                    }
                }
                for (int i = 0; i < 8; ++i)
                {
                    buttons[i].transform.Translate(-0.02f, 0, 0);
                }
            }
        }
    }
}